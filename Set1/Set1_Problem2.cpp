#include "Set1_Problem2.h"

using namespace std;

namespace Set1 {
    void set1_problem2_Main() {
        int a = 1;
        int b = 2;
        int c = 3;
        cout << "a + b = " << a + b << endl;
        cout << "c = " << c << endl;
        return;
    }

    void set1_problem2_runtime() {
        int a = 1;
        int b = 0;
        cout << "int division by zero:" << a / b << endl;

        double x = 1.0;
        double y = 0.0;
        cout << "double division by zero:" << x / y << endl;
        return;
    }
}
