#include "Set1_Problem1.h"

using namespace std;

namespace Set1 {
    void problem1_print() {
        string question1 = "C++ uses the operators << and >> for input and output, respectively.";
        string question2 = "A step-by-step procedure that accomplishes a desired task is called an algorithm.";
        string question3 = "The operating system consists of programs and has two main purposes. First, the operating system oversees and coordinates the resources of the machine as a whole. Second, the operating system provides tools to users.";
        string question4 = "The compiler takes source code and produces object code.";
        string question5 = "In the code std::cout, cout is the standard output stream, :: is the scope resolution operator and std is the namespace for the standard library.";
        string question6 = "A flowchart is a graphical means for displaying an algorithm.";
        string question7 = "int price, change, dimes, pennies; This declares four integer variables. These hold the values to be manipulated.";
        string question8 = "The text uses Bell Labs style. There is one blank line following the #includes, and one between the declarations and statements in the body of main(). An indentation of two, three, four, five, or eight spaces is common.";
        string question9 = "In Windows and in UNIX, a control-c is commonly used to effect an interrupt.";
        string question10 = "A common error is to misspell a variable name or forget to declare it.";

        cout << "1. " << question1 << endl;
        cout << "2. " << question2 << endl;
        cout << "3. " << question3 << endl;
        cout << "4. " << question4 << endl;
        cout << "5. " << question5 << endl;
        cout << "6. " << question6 << endl;
        cout << "7. " << question7 << endl;
        cout << "8. " << question8 << endl;
        cout << "9. " << question9 << endl;
        cout << "10. " << question10 << endl;

        return;
    }
}



