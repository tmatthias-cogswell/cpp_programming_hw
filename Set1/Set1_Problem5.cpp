#include "Set1_Problem5.h"

using namespace std;

namespace Set1 {
    void problem5_print() {
        string OOP_Discuss = "Object Oriented Programming is a programming paradigm that utilizes objects, defined by classes as a blueprint, to model data and manipulate data.\n"
                "This is opposed to a program that simply takes an input, process it, and outputs the manipulated data."
                "OOP also relies heavily on inheritance, where a class can be inherited by a subclass for variations in the data model.";

        string discuss_inline = "An inline function is a function that will be replaced by its call with the full function code at compile time.";

        string discuss_constructors = "A constructor is a function that is run whenever a new object is created from the class constructor definition.";
        string discuss_deconstructors = "A deconstructor is a function that is run whenever an object is deleted.";

        cout << OOP_Discuss << endl;
        cout << discuss_inline << endl;
        cout << discuss_constructors << endl;
        cout << discuss_deconstructors << endl;
    }
}
