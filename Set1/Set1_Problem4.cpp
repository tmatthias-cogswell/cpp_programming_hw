#include "Set1_Problem4.h"

using namespace std;

namespace Set1 {
    void problem4_print() {
        string discuss_address = "The & symbol is denotes an address in memory of a variable";
        string discuss_call_by_value = "Calling by Value describes passing the value of a variable to a function.";
        string discuss_call_by_pointers = "Calling by a Pointer describes using a pointer to a value to a function.";
        string discuss_call_by_reference = "Calling by reference describes using the address of a value for a function call.";

        cout << discuss_address << endl;
        cout << discuss_call_by_value << endl;
        cout << discuss_call_by_pointers << endl;
        cout << discuss_call_by_reference << endl;

        return;
    }
}
