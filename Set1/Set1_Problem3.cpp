#include "Set1_Problem3.h"

using namespace std;

namespace Set1 {
    void problem3_print() {
        string question1 = "A type in C++ that C and early C++ does not have is bool or wchar_t.";
        string question2 = "bool and wchar_t are new types, the rest are access keywords.";
        string question3 = "The // comment is a end of line comment, which is less prone to error and easier to use than /* to */.";
        string question4 = "true and false can be converted to 1 and 0.";
        string question5 = "static_cast<> is convertible and portable, whereas reinterpret_cast<> is more prone to error.";
        string question6 = "C++ uses the semicolon as a statement terminator. Also it is a sequence point, where all the actions must be accomplished before continuing.";
        string question7 = "C++ for loops can have a local declaration of a loop variable, and the second expression can be a boolean statement.";
        string question8 = "The goto statement should never be used.";
        string question9 = "A for loop without a condition will usually result in an infinite loop, as the condition is always true.";
        string question10 = "It is customary in C++, to place an opening brace on the same line as the start- ing keyword for a statement, such as an if or for. The closing brace lines up with the first character of this keyword.";

        cout << "1. " << question1 << endl;
        cout << "2. " << question2 << endl;
        cout << "3. " << question3 << endl;
        cout << "4. " << question4 << endl;
        cout << "5. " << question5 << endl;
        cout << "6. " << question6 << endl;
        cout << "7. " << question7 << endl;
        cout << "8. " << question8 << endl;
        cout << "9. " << question9 << endl;
        cout << "10. " << question10 << endl;

        return;

    }
}
