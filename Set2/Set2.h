/*
Due Thursday October 1, 2015
Problem 1: Do review questions on chapter 4 p. 178 PohlF.
Problem 2: Do exercise #6 p.179 Pohl
Problem 3: Do problem #10 p.180 Pohl
Problem 4: Why do we create the copy constructor? Discuss difference of deep copy and shallow copy? What is the reference counter? Why do we use it?
*/

#ifndef CPP_CLION_SET2_H
#define CPP_CLION_SET2_H

#include "Set2_Problem1.h"
#include "Set2_Problem2.h"
#include "Set2_Problem3.h"
#include "Set2_Problem4.h"

#endif //CPP_CLION_SET2_H