#include "Set2_Problem3.h"

using namespace std;

namespace Set2 {
    my_deque::my_deque(int in_size) {
        count = 0;
        size = in_size;
        my_stack = new int[size];
    }

    void my_deque::print_stack() {
        cout << "Printing: " << my_stack << endl;
        for (int i = 0; i != count; i++) {
            cout << *(top_of() + (i * sizeof(int *))) << endl;
        }
    }

    void my_deque::push_t(int num) {
        if (empty()) {
            *my_stack = num;
            bottom = top = my_stack;
            count++;
        }

        else if (full()) {
            cout << "Queue is full!" << endl;
        }

        else {
            for (int j = 0; j != count; j++) {
                int *next = top_of() + ((j + 1) * sizeof(int *));
                *next = *(top_of() + (j * sizeof(int *)));
                *my_stack = *top = num;
            }
            count++;
        }
        bottom = bottom_of();
    }

    void my_deque::push_b(int num) {
        if (empty()) {
            *my_stack = num;
            bottom = top = my_stack;
            count++;
        }

        else if (full()) {
            cout << "Queue is full!" << endl;
        }

        else {
            bottom = bottom_of() + (sizeof(int *));
            *bottom = num;
            count++;
        }


    }

    void my_deque::pop_t() {
        for (int j = 0; j < count; j++) {
            int *following = top_of() + (j * sizeof(int *));
            *following = *(following + (sizeof(int *)));
        }
        count--;
    }

    void my_deque::pop_b() {
        *(bottom_of()) = 0;
        count--;
    }

    int *my_deque::top_of() {
        return my_stack;
    }

    int *my_deque::bottom_of() {
        return (my_stack + ((count - 1) * sizeof(int *)));
    }

    bool my_deque::empty() {
        return count == 0 ? true : false;
    }

    bool my_deque::full() {
        return count == size ? true : false;
    }

/*    my_deque::~my_deque() {
        cout << "Destroying" << endl;
    }*/
}
