#include "Set2_Problem2.h"

using namespace std;

namespace Set2 {
    void problem2_test_stack() {
        problem2_ch_stack s;
        char str[40] = { "My name is Thomas Matthias" };
        int i=0;
        cout << str << endl;
        s.reset(); // s.top = EMPTY; is illegal
        while (str[i] && !s.full())
            s.push(str[i++]);
        while (!s.empty()) // print the reverse
            cout << s.pop();
        cout << endl;
    }
}