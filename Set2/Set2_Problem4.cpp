#include "Set2_Problem4.h"

#include <iostream>

namespace Set2 {

    void problem4_print() {
        std::string string1 = "A Copy Constructor is a special constructor for creating a copy of an already existing object.";
        std::string string2 = "A shallow copy is a method to duplicate as little as possible. A shallow copy of an object is a copy of it's structure, not the elements. With a shallow copy, two objects now share the individual elements.";
        std::string string3 = "Deep copies duplicate everything. A deep copy of an object is two objects with all of the elements in the original object duplicated.";
        std::string string4 = "A Reference Counter simply counts how often a reference to the object is used, and will decrement when a reference is disposed of.";

        std::cout << "1. " << string1 << std::endl;
        std::cout << "2. " << string2 << std::endl;
        std::cout << "3. " << string3 << std::endl;
        std::cout << "4. " << string4 << std::endl;
    }
}
