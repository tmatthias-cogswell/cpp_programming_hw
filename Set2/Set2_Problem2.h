#ifndef CPP_CLION_SET2_PROBLEM2_H
#define CPP_CLION_SET2_PROBLEM2_H

#include <iostream>

namespace Set2 {
    class problem2_ch_stack {
    public:
        void reset() { top = EMPTY; }
        void push(char c) { s[++top] = c; }
        char pop() { return s[top--];}
        char top_of() const { return s[top]; }
        bool empty() const { return (top == EMPTY); }
        bool full() const { return (top == FULL); }

    private:
        enum {max_len = 100, EMPTY = -1, FULL = max_len - 1};
        char s[max_len];
        int top;
    };

    void problem2_test_stack();
}

#endif //CPP_CLION_SET2_PROBLEM2_H
