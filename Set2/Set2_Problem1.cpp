#include "Set2_Problem1.h"

namespace Set2 {
    void problem1_print() {
        std::string question1 = "In C++, the structure name, or tag name, is a type.";
        std::string question2 = "Member functions that are defined within class are implicitly private.";
        std::string question3 = "A function invocation w1.print(); means that print is a member function.";
        std::string question4 = "A private member (can or cannot) can be used by a member function of that class.";
        std::string question5 = "The static modifier used in declaring a data member means that the data member is independent of any given class variable.";
        std::string question6 = "The preferred style is to have members of public access first and members of private access declared last in a class declaration.";
        std::string question7 = "A stack is a LIFO container. A container is a data structure whose main pur- pose is to store and retrieve a large number of values.";
        std::string question8 = "LIFO means last in first out.";

        std::cout << "1. " << question1 << std::endl;
        std::cout << "2. " << question2 << std::endl;
        std::cout << "3. " << question3 << std::endl;
        std::cout << "4. " << question4 << std::endl;
        std::cout << "5. " << question5 << std::endl;
        std::cout << "6. " << question6 << std::endl;
        std::cout << "7. " << question7 << std::endl;
        std::cout << "8. " << question8 << std::endl;

        return;

    }
}
