#ifndef CPP_CLION_SET2_PROBLEM3_H
#define CPP_CLION_SET2_PROBLEM3_H

#include <iostream>

namespace Set2 {
    class my_deque {
    public:
        my_deque(int);
        //~my_deque();
        void reset();
        void push_t(int);
        void push_b(int);
        void pop_t();
        void pop_b();
        void print_stack();
        int * top_of();
        int * bottom_of();
        bool empty();
        bool full();

    private:
        int *bottom;
        int *top;
        int size;
        int count;
        int *my_stack;
    };

    void problem3_print();
}
#endif //CPP_CLION_SET2_PROBLEM3_H
