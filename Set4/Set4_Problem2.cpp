#include "Set4_Problem2.h"

namespace Set4 {

    cpx_num::cpx_num() {
        int real = 0;
        int imag = 0;
    }

    cpx_num::cpx_num(int r_in, int i_in) {
        double real = r_in;
        double imag = i_in;
    }

    cpx_num::cpx_num(const cpx_num &) {
        double real = cpx_num::real;
        double imag = cpx_num::imag;
    }

    cpx_num cpx_num::operator+(const cpx_num &in_cpx) {
        double result_real = real + in_cpx.real;
        double result_imag = imag + in_cpx.imag;
        return cpx_num(result_real, result_imag);
    }

    cpx_num cpx_num::operator-(const cpx_num &in_cpx) {
        double result_real = real - in_cpx.real;
        double result_imag = imag - in_cpx.imag;
        return cpx_num(result_real, result_imag);
    }

    cpx_num cpx_num::operator*(const cpx_num &in_cpx) {
        double result_real = (real * in_cpx.real) - (imag * in_cpx.imag);
        double result_imag = (real * in_cpx.imag) + (imag * in_cpx.real);
        return cpx_num(result_real, result_imag);
    }

    cpx_num cpx_num::operator/(const cpx_num &in_cpx) {
        double numer_real = (real * in_cpx.real) + (imag * in_cpx.imag);
        double numer_imag = (imag * in_cpx.real) - (real * in_cpx.imag);
        float denom = (in_cpx.real * in_cpx.real) * (in_cpx.imag * in_cpx.imag);
        float result_real = numer_real / denom;
        float result_imag = numer_imag / denom;
        return cpx_num(result_real, result_imag);
    }

    cpx_num cpx_num::get_conj(const cpx_num &in_cpx) {
        real = in_cpx.real;
        imag = in_cpx.imag * -1;
        return cpx_num(real, imag);
    }
}