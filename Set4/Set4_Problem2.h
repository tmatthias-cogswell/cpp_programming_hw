#ifndef CPP_CLION_SET4_PROBLEM2_H
#define CPP_CLION_SET4_PROBLEM2_H

namespace Set4 {
    class cpx_num {
    private:
        int real;
        int imag;
    public:
        cpx_num();

        cpx_num(int r_in, int i_in);

        cpx_num(const cpx_num &);

        cpx_num operator+(const cpx_num &);

        cpx_num operator-(const cpx_num &);

        cpx_num operator*(const cpx_num &);

        cpx_num operator/(const cpx_num &);

        cpx_num get_conj(const cpx_num &);

    };
}

#endif //CPP_CLION_SET4_PROBLEM2_H
