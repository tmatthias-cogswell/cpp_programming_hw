#include <iostream>
#include "Set1/Set1.h"
#include "Set2/Set2.h"
#include "Set3/Set3.h"

using namespace std;

void Set1_Answers();

void Set2_Answers();

void Set3_Answers();

int main() {

    int answer_num;
    cout << "Which homework answer set to display?";
    cin >> answer_num;

    switch (answer_num) {
        case 1:
            Set1_Answers();
            break;
        case 2:
            Set2_Answers();
            break;
        case 3:
            Set3_Answers();
            break;
        default:
            cout << "Invalid.  Exiting..." << endl;
    }
    return 0;
}

void Set1_Answers() {
    cout << "\nProblem 1: " << endl;
    Set1::problem1_print();
    cout << "\nProblem 2: " << endl;
    Set1::set1_problem2_Main();
    Set1::set1_problem2_runtime();
    cout << "\nProblem 3: " << endl;
    Set1::problem3_print();
    cout << "\nProblem 4: " << endl;
    Set1::problem4_print();
    cout << "\nProblem 5: " << endl;
    Set1::problem5_print();
    return;
}

void Set2_Answers() {
    cout << "Problem 1: " << endl;
    Set2::problem1_print();

    cout << "Problem 2: " << endl;
    Set2::problem2_test_stack();

    cout << "Problem 3: " << endl;
    Set2::my_deque test_q(4);
    test_q.push_t(7);
    test_q.push_t(11);
    test_q.print_stack();
    test_q.push_b(8);
    test_q.print_stack();
    test_q.push_b(12);
    test_q.print_stack();
    test_q.push_t(9);
    test_q.pop_t();
    test_q.print_stack();
    test_q.pop_b();
    test_q.print_stack();

    cout << "Problem 4: " << endl;
    Set2::problem4_print();

    return;
}

void Set3_Answers() {
    cout << "Deque Example: " << endl;
    Set3::Set3_Deque example_q(10);
    example_q.push_t(2);
    example_q.push_t(3);
    example_q.push_t(4);
    example_q.push_t(5);
    example_q.push_t(6);
    example_q.push_t(7);
    example_q.push_t(8);
    example_q.push_t(9);
    example_q.push_b(10);
    example_q.push_b(1);
    example_q.push_b(0);
    example_q.pop_b();
    example_q.push_b(0);
    example_q.print_stack();

    cout << "Simple Linked List Example: " << endl;
    Set3::LinkedList example_ll;
    example_ll.addNode(2);
    example_ll.addNode(3);
    example_ll.addNode(4);
    example_ll.display();
}

