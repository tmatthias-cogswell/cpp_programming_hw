// For this assignment, I wanted to create the simplest linked list I possibly could.
// Considering my deque implementation covers stacks, linked lists, and obviously queues, and is quite complex, this is as simple as it gets.

#ifndef CPP_CLION_LINKEDLIST_H
#define CPP_CLION_LINKEDLIST_H

namespace Set3 {
    class LinkedList {

    public:
        LinkedList();
        ~LinkedList();
        void addNode(int);
        void display();
        struct Node;

    };
}

#endif
