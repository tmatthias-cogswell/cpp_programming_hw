// This source file defines the functions for the Set3_Deque class.
// The usage instructions can be found in the Set3_Deque.h file.

#include "Set3_Deque.h"
#include <iostream>

using namespace std;

namespace Set3 {
    Set3_Deque::Set3_Deque(int in_size) {
        count = 0;
        size = in_size;
        my_stack = new int[size];
    }

    void Set3_Deque::print_stack_2() {
        cout << "Printing: " << my_stack << endl;
        for (int i = 0; i != count; i++) {
            cout << *(top_of() + (i * sizeof(int *))) << endl;
        }
    }

    void Set3_Deque::print_stack() {
        cout << "Printing: " << my_stack << endl;
        for (int i = 0; i != count; i++) {
            if (i == 0) {
                cout << "[" << *(top_of()) << ", ";
            }

            else if (i == (count - 1)) {
                bottom = bottom_of();
                cout << *bottom << "]" << endl;
            }

            else {
                cout << *(top_of() + (i * sizeof(int *))) << ", ";
            }
            bottom_of();
        }
    }

    void Set3_Deque::push_t(int num) {
        if (empty()) {
            *my_stack = num;
            bottom = top = my_stack;
            count++;
        }

        else if (count == 1) {
            int *next = (top_of() + (count * sizeof(int *)));
            *next = *(top_of());
            *my_stack = num;
            bottom = bottom_of();
            count++;
        }

        else if (full()) {
            cout << "Queue is full!" << endl;
        }

        else {
            for (int j = (count - 1); j != 0; j--) {
                int hold_value = *(top_of() + (j * sizeof(int *)));
                int *shift_to = (top_of() + (j * sizeof(int *)));
                int shift_value = *(top_of() + ((j - 1) * sizeof(int *)));
                *shift_to = shift_value;
                int *shift_over = (top_of() + ((j + 1) * sizeof(int *)));
                *shift_over = hold_value;
            }
            *my_stack = num;
            count++;
        }
    }

    void Set3_Deque::push_b(int num) {
        if (empty()) {
            *my_stack = num;
            bottom = top = my_stack;
            count++;
        }

        else if (full()) {
            cout << "Queue is full!" << endl;
        }

        else {
            bottom = bottom_of() + (sizeof(int *));
            *bottom = num;
            count++;
        }
        bottom = bottom_of();
    }

    void Set3_Deque::pop_t() {
        for (int j = 0; j < count; j++) {
            int *following = top_of() + (j * sizeof(int *));
            *following = *(following + (sizeof(int *)));
        }
        count--;
    }

    void Set3_Deque::pop_b() {
        *(bottom_of()) = 0;
        count--;
    }

    int *Set3_Deque::top_of() {
        return my_stack;
    }

    int *Set3_Deque::bottom_of() {
        return (my_stack + ((count - 1) * sizeof(int *)));
    }

    bool Set3_Deque::empty() {
        return count == 0 ? true : false;
    }

    bool Set3_Deque::full() {
        return count == size ? true : false;
    }

    Set3_Deque::~Set3_Deque() {
        delete[] my_stack;
    }
}
