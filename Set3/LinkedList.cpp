#include <cstdlib>
#include <iostream>
#include "LinkedList.h"

namespace Set3 {
    struct LinkedList::Node {
        int data;
        struct Node *next;
    };
    LinkedList::Node *head = NULL;

    LinkedList::LinkedList(){}

    LinkedList::~LinkedList(){}

    void LinkedList::addNode(int input) {
        Node *new_node = new Node;
        new_node->data = input;
        new_node->next = NULL;

        if (head == NULL) {
            head = new_node;
            return;
        }

        Node *cur = head;
        while(cur){
            if(cur->next == NULL){
                cur->next = new_node;
                return;
            }
            cur = cur->next;
        }
    }

    void LinkedList::display() {
        struct Node *list = head;
        while(list) {
            std::cout << list->data <<" ";
            list = list->next;
        }
        std::cout << std::endl;
    }
}

