// This Header File sets the parameters for a "Doubled Ended Queue" class.
// This deque takes only 1 creation argument, the initial size of the queue.
// Values can be added via the push_t and push_b methods, with an integer as an argument.
// This class does not use nodes, but rather uses pointers to track the location of values ahead of or before in the queue.

#ifndef CPP_CLION_SET3_DEQUE_H
#define CPP_CLION_SET3_DEQUE_H

namespace Set3 {
    class Set3_Deque {
    public:
        Set3_Deque(int);
        ~Set3_Deque();
        void reset();
        void push_t(int);
        void push_b(int);
        void pop_t();
        void pop_b();
        void print_stack();
        void print_stack_2();
        int * top_of();
        int * bottom_of();
        bool empty();
        bool full();

    private:
        int *bottom;
        int *top;
        int size;
        int count;
        int *my_stack;
    };
}


#endif //CPP_CLION_SET3_DEQUE_H
